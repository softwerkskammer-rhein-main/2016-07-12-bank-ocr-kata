package de.java.coding.dojo;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ScanMachineTest {

    @org.junit.Test
    public void parseALineWithZeroDigits() throws Exception {
        String entry =
                " _  _  _  _  _  _  _  _  _ \n" +
                "| || || || || || || || || |\n" +
                "|_||_||_||_||_||_||_||_||_|\n" +
                "                           ";

        ScanMachine scanMachine = new ScanMachine();
        String result = scanMachine.parseEntry(entry);

        assertEquals("000000000", result);
    }

    @org.junit.Test
    public void parseALineWithDigitsContainingOnes() throws Exception {
        String entry =
                "                           \n" +
                "  |  |  |  |  |  |  |  |  |\n" +
                "  |  |  |  |  |  |  |  |  |\n" +
                "                           ";

        ScanMachine scanMachine = new ScanMachine();
        String result = scanMachine.parseEntry(entry);

        assertEquals("111111111", result);
    }

    @Test
    public void parseFirstDigit() throws Exception {
       String entry =
               "                           \n" +
               "  |  |  |  |  |  |  |  |  |\n" +
               "  |  |  |  |  |  |  |  |  |\n" +
               "                           ";

        ScanMachine scanMachine = new ScanMachine();
        String result = scanMachine.parseEntryFirstDigit(entry);

        assertEquals("1", result);

    }

    @Test
    public void getFirstDigitAsString() throws Exception {

        String entry =
                "                           \n" +
                "  |  |  |  |  |  |  |  |  |\n" +
                "  |  |  |  |  |  |  |  |  |\n" +
                "                           ";
        ScanMachine scanMachine = new ScanMachine();
        String result = scanMachine.getFirstDigitAsString(entry);

        assertEquals("     |  |", result);

    }
}
