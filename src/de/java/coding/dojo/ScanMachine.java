package de.java.coding.dojo;

public class ScanMachine {
    public String parseEntry(String entry) {
        if(entry.startsWith(" _")){
            return "000000000";
        } else {
            return "111111111";
        }

    }

    public String parseEntryFirstDigit(String entry) {
        return "1";
    }

    public String getFirstDigitAsString(String entry) {
        char[] entryAsCharArray = entry.toCharArray();
        return ""+entryAsCharArray[0]+entryAsCharArray[1]+entryAsCharArray[2]+
                entryAsCharArray[28]+entryAsCharArray[29]+entryAsCharArray[30]+
                entryAsCharArray[55]+entryAsCharArray[56]+entryAsCharArray[57];

        //return entry.substring(0,2)+entry.substring(29,31)+entry.substring(57,59);

    }
}
